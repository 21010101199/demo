import 'package:demo/api/api_demo_http.dart';
import 'package:demo/dimension_screens.dart';
import 'package:demo/screen_03.dart';
import 'package:flutter/material.dart';

class ScreenZeroTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            color: Colors.grey.withOpacity(0.2),
            child: Container(
              margin: EdgeInsets.only(
                  left: Dimensions.width10,
                  right: Dimensions.width10,
                  top: Dimensions.height10),
              // color: Colors.grey.withOpacity(0.2),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Hello, Kristin",
                        style: TextStyle(
                            fontSize: Dimensions.font20,
                            fontWeight: FontWeight.w500),
                      ),
                      Icon(
                        Icons.search,
                        size: Dimensions.iconsize28,
                      ),
                    ],
                  ),
                  Text(
                    "Let's Create a",
                    style: TextStyle(
                        fontSize: Dimensions.font30, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Perfect Moment",
                    style: TextStyle(
                        fontSize: Dimensions.font30, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: Dimensions.height15,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        getCustomizedContainer("Trending",context),
                        getCustomizedContainer("Workshop",context),
                        getCustomizedContainer("Anniversary",context),
                        getCustomizedContainer("Marriage",context),
                        getCustomizedContainer("Marriage Anniversary",context),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: Dimensions.height15,
                  ),
                  Container(
                      margin: EdgeInsets.only(
                          left: Dimensions.width15, right: Dimensions.width15),
                      height: Dimensions.height120,
                      decoration: BoxDecoration(
                          // border: Border.all(width: 1),
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.circular(Dimensions.radius15)),
                      child: Column(
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: (){
                                Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                                  return ScreenZeroThree();
                                },));
                              },
                              child: getCUstomizedMiddleContainer(
                                  "assets/images/fishing.jpeg",
                                  "Fishing",
                                  "Family",
                                  "230"),
                            ),
                          ),
                        ],
                      )),
                  SizedBox(
                    height: Dimensions.height15,
                  ),
                  Container(
                      margin: EdgeInsets.only(
                          left: Dimensions.width15, right: Dimensions.width15),
                      height: Dimensions.height120,
                      decoration: BoxDecoration(
                          // border: Border.all(width: 1),
                          color: Colors.white,
                          borderRadius:
                              BorderRadius.circular(Dimensions.radius15)),
                      child: Column(
                        children: [
                          Expanded(
                            child: getCUstomizedMiddleContainer(
                                "assets/images/cycling.jpeg",
                                "Touring",
                                "Family",
                                "230"),
                          ),
                        ],
                      )),
                  SizedBox(
                    height: Dimensions.height15,
                  ),
                  Text(
                    "Our Offerings",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: Dimensions.font18),
                  ),
                  SizedBox(
                    height: Dimensions.height15,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        getCustomizedBlock(Icons.play_arrow_outlined, "virtual"),
                        getCustomizedBlock(
                            Icons.sports_volleyball_outlined, "wedding"),
                        getCustomizedBlock(Icons.heart_broken, "Anniversary"),
                        getCustomizedBlock(
                            Icons.baby_changing_station_outlined, "Baby"),
                        getCustomizedBlock(Icons.access_time, "Time")
                      ],
                    ),
                  ),
                  SizedBox(
                    height: Dimensions.height40,
                  ),
                  Container(
                    height: Dimensions.height70,
                    // color: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(Icons.home),
                        Container(
                          color: Colors.grey.withOpacity(0.3),
                          child: Icon(Icons.menu,color: Colors.white.withOpacity(0.5),),
                        ),
                        Icon(Icons.groups,color: Colors.grey,),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.orange,
                            borderRadius:
                                BorderRadius.circular(Dimensions.radius30),
                          ),
                          child: Icon(Icons.girl_sharp),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget getCustomizedContainer(text,context) {
    return Container(
      margin: EdgeInsets.only(right: Dimensions.width10),
      padding:
          EdgeInsets.only(left: Dimensions.width10, right: Dimensions.width10),
      decoration: BoxDecoration(
        color: text != "Trending"
            ? Colors.grey.withOpacity(0.4)
            : Color(0xFF021B52),
        borderRadius: BorderRadius.circular(Dimensions.radius30),
      ),
      child: TextButton(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) => ApiDemoHttp(),));
        },
        child: Text(
          text,
          style: text == "Trending"
              ? TextStyle(color: Colors.yellow)
              : TextStyle(color: Colors.black.withOpacity(0.5)),
        ),
      ),
    );
  }

  Widget getCUstomizedMiddleContainer(imgUrl, text, description, price) {
    return Row(
      children: [
        Expanded(
          child: Container(
            decoration: BoxDecoration(
                color: Colors.grey.withOpacity(0.2),
                // border: Border.all(width: 2),
                borderRadius: BorderRadius.circular(Dimensions.radius15),
                image: DecorationImage(
                    image: AssetImage(imgUrl), fit: BoxFit.cover)),
          ),
        ),
        Expanded(
          flex: 2,
          child: Container(
            margin: EdgeInsets.only(
                left: Dimensions.width10,
                top: Dimensions.height10,
                right: Dimensions.width10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  text,
                  style: TextStyle(
                      fontSize: Dimensions.font25, fontWeight: FontWeight.bold),
                ),
                Text(
                  description,
                  style: TextStyle(
                      fontSize: Dimensions.font15, color: Colors.grey),
                ),
                // SizedBox(
                //   height: Dimensions.height10,
                // ),
                Text(
                  "Start from",
                  style: TextStyle(
                      fontSize: Dimensions.font15, color: Colors.grey),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "\$$price",
                      style: TextStyle(
                          fontSize: Dimensions.font25,
                          color: Colors.orange,
                          fontWeight: FontWeight.bold),
                    ),
                    CircleAvatar(
                      radius: Dimensions.radius15,
                      backgroundColor: Colors.grey.withOpacity(0.3),
                      child: Image.asset("assets/images/skip-track.png",
                          height: Dimensions.height15),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget getCustomizedBlock(IconData icon, text) {
    return Column(
      children: [
        Container(
          height: Dimensions.height50,
          width: Dimensions.height50,
          margin: EdgeInsets.only(
              left: Dimensions.width15,
              right: Dimensions.width15,
              bottom: Dimensions.height10),
          decoration: BoxDecoration(
              // border: Border.all(width: 1),
              color: Colors.white,
              borderRadius: BorderRadius.circular(Dimensions.radius15)),
          child: Icon(
            icon,
            size: Dimensions.iconsize28,
            color: Colors.black.withOpacity(0.4),
          ),
        ),
        Text(
          text,
          style: TextStyle(fontSize: Dimensions.font15, color: Colors.black87),
        ),
      ],
    );
  }
}
