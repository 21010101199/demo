import 'package:demo/dimension_screens.dart';
import 'package:demo/screen_02.dart';
import 'package:flutter/material.dart';

class ScreenZeroOne extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.only(
                left: Dimensions.width10, right: Dimensions.width10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Container(
                    child: Image.asset(
                      "assets/images/women_guitar.png",
                      height: Dimensions.height340,
                    ),
                  ),
                ),
                Text(
                  "Welcome,",
                  style: TextStyle(
                      fontSize: Dimensions.font30, fontWeight: FontWeight.bold),
                ),
                Text(
                  "You have been missed!",
                  style: TextStyle(
                      fontSize: Dimensions.font20, fontWeight: FontWeight.w500),
                ),
                Center(
                  child: Container(
                    margin: EdgeInsets.only(top: Dimensions.height15),
                    width: Dimensions.screenWidth,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Dimensions.radius15),
                        color: Color(0xFF021B52)),
                    child: TextButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                          return ScreenZeroTwo();
                        },));
                      },
                      child: Text(
                        "Sign In",
                        style: TextStyle(
                            fontSize: Dimensions.font20,
                            color: Colors.yellowAccent),
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    margin: EdgeInsets.only(top: Dimensions.height10),
                    child: Text(
                      "or",
                      style: TextStyle(
                          fontSize: Dimensions.font18, color: Colors.grey),
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    margin: EdgeInsets.only(top: Dimensions.height15),
                    width: Dimensions.screenWidth,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Dimensions.radius15),
                        color: Colors.white),
                    child: TextButton(
                      onPressed: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircleAvatar(
                            child: Image.asset("assets/images/google_icon.jpeg",fit: BoxFit.cover
                              ,),
                            radius: Dimensions.radius14,
                          ),
                          SizedBox(width: Dimensions.width10,),
                          Text(
                            "Continue With Google",
                            style: TextStyle(
                                fontSize: Dimensions.font20,
                                color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Center(
                  child: Container(
                    margin: EdgeInsets.only(top: Dimensions.height15),
                    width: Dimensions.screenWidth,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Dimensions.radius15),
                        color: Colors.white),
                    child: TextButton(
                      onPressed: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(Icons.apple,color: Colors.black,size: Dimensions.iconsize28,),
                          SizedBox(width: Dimensions.width15,),
                          Text(
                            "Continue With Apple",
                            style: TextStyle(
                                fontSize: Dimensions.font20,
                                color: Colors.black),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: Dimensions.height10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Don't have account?",style: TextStyle(fontSize: Dimensions.font18,color: Colors.grey),),
                      SizedBox(width: Dimensions.width10,),
                      Text("Register",style: TextStyle(fontSize: Dimensions.font18,color: Colors.black,fontWeight: FontWeight.bold),),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
