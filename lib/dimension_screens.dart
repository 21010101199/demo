import 'package:get/get.dart';

class Dimensions{
  static double screenHeight = Get.context!.height;
  static double screenWidth = Get.context!.width;

  //margin and padding
  static double width10 = screenWidth/41.1;
  static double height10 = screenHeight/68.3;
  static double width25 = screenWidth/16.44;
  static double width15 = screenWidth/27.4;
  static double height15 = screenHeight/45.53;
  static double height50 = screenHeight/13.66;
  static double width30 = screenWidth/13.7;
  static double height40 = screenHeight/17.07;
  static double width60 = screenWidth/6.85;
  static double width120 = screenWidth/3.425;
  static double height120 = screenHeight/5.69;
  static double height70 = screenHeight/9.75;
  static double height25 = screenHeight/27.32;



  //girl image size
  static double height340 = screenHeight/2.008;



  //radius
  static double radius30 = screenHeight/22.76;
  static double radius15 = screenHeight/45.53;
  static double radius14 = screenHeight/48.78;
  static double radius10 = screenHeight/68.3;



  //font
  static double font30 = screenHeight/22.76;
  static double font15 = screenHeight/45.53;
  static double font25 = screenHeight/27.32;
  static double font20 = screenHeight/34.15;
  static double font18 = screenHeight/37.94;



  //icon size
  static double iconsize24 = screenHeight/28.45;
  static double iconsize28 = screenHeight/24.39;


}