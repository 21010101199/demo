import 'package:demo/dimension_screens.dart';
import 'package:flutter/material.dart';

class ScreenZeroThree extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Stack(
        alignment: Alignment.center,
        fit: StackFit.expand,
        children: [
          Image.asset(
            "assets/images/fishing.jpeg",
            fit: BoxFit.cover,
          ),
          Positioned(
            bottom: 0,
            left: 5,
            right: 5,
            child: Container(
              width: Dimensions.screenWidth,
              margin: EdgeInsets.only(bottom: Dimensions.height15),
              decoration: BoxDecoration(
                color: Colors.green,
                borderRadius: BorderRadius.circular(Dimensions.radius30),
              ),
              child: TextButton(
                  onPressed: () {
                    showModalBottomSheet(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(Dimensions.radius30),
                            topRight: Radius.circular(Dimensions.radius30)),
                      ),
                      context: context,
                      builder: (context) {
                        return Container(
                          margin: EdgeInsets.only(
                              left: Dimensions.width15,
                              right: Dimensions.width15),
                          child: Stack(
                            children: [
                              SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Container(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Center(
                                          child: Text(
                                        "------------",
                                      )),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Family",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: Dimensions.font18),
                                          ),
                                          Text(
                                            "Start from",
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontSize: Dimensions.font18),
                                          )
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Fishing",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: Dimensions.font25,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            "\$230",
                                            style: TextStyle(
                                                color: Colors.orange,
                                                fontSize: Dimensions.font20),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: Dimensions.height15,
                                      ),
                                      Text(
                                        "Description",
                                        style: TextStyle(
                                            fontSize: Dimensions.font20,
                                            fontWeight: FontWeight.w600),
                                      ),
                                      SizedBox(
                                        height: Dimensions.height10,
                                      ),
                                      Text(
                                          "Lorem ipsum is simply a dummy text which i used just as it is and everywhere"),
                                      SizedBox(
                                        height: Dimensions.height10,
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: Dimensions.width15,
                                            right: Dimensions.width15,
                                            top: Dimensions.height10),
                                        height: Dimensions.height70,
                                        decoration: BoxDecoration(
                                            color: Colors.grey.withOpacity(0.2),
                                            borderRadius: BorderRadius.circular(
                                                Dimensions.radius30)),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  "\$230/",
                                                  style: TextStyle(
                                                      fontSize: Dimensions.font20,
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.orange),
                                                ),
                                                Text(
                                                  "1 Day",
                                                  style: TextStyle(
                                                      fontSize: Dimensions.font15,
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.black),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                              children: [
                                                Text(" "),
                                                Container(
                                                  height: Dimensions.height25,
                                                  width: Dimensions.height25,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            Dimensions.radius10),
                                                    color: Colors.black,
                                                    // image: DecorationImage(image: AssetImage("assets/images/tick.png",),fit: BoxFit.cover)
                                                  ),
                                                  child: Image.asset(
                                                      "assets/images/tick.png",
                                                      color: Colors.yellow),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: Dimensions.width10),
                                              child: Text(
                                                "4 Person Labuan Baja sea with sailing",
                                                style:
                                                    TextStyle(color: Colors.black,fontSize: 7),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                            left: Dimensions.width15,
                                            right: Dimensions.width15,
                                            top: Dimensions.height10),
                                        height: Dimensions.height70,
                                        decoration: BoxDecoration(
                                            color: Colors.grey.withOpacity(0.2),
                                            borderRadius: BorderRadius.circular(
                                                Dimensions.radius30)),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  "\$230 ",
                                                  style: TextStyle(
                                                      fontSize: Dimensions.font20,
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.orange),
                                                ),
                                                Text(
                                                  "1 Day",
                                                  style: TextStyle(
                                                      fontSize: Dimensions.font15,
                                                      fontWeight: FontWeight.bold,
                                                      color: Colors.black),
                                                ),
                                              ],
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                              children: [
                                                Text(" "),
                                                Container(
                                                  height: Dimensions.height25,
                                                  width: Dimensions.height25,
                                                  decoration: BoxDecoration(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            Dimensions.radius10),
                                                    color: Colors.grey,
                                                    // image: DecorationImage(image: AssetImage("assets/images/tick.png",),fit: BoxFit.cover)
                                                  ),
                                                  // child: Image.asset(
                                                  //     "assets/images/tick.png",
                                                  //     color: Colors.yellow),
                                                ),
                                              ],
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(
                                                  left: Dimensions.width10),
                                              child: Text(
                                                "4 Person Labuan Baja sea with sailing",
                                                style:
                                                    TextStyle(color: Colors.black),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 0,
                                right: 5,
                                left: 5,
                                child: Container(
                                  height: 100,
                                  width: Dimensions.screenWidth,
                                  color: Colors.white,
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Total",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: Dimensions.font25),
                                          ),
                                          Text(
                                            "\$230",
                                            style: TextStyle(
                                                fontSize: Dimensions.font20,
                                                color: Colors.orange),
                                          )
                                        ],
                                      ),
                                      Container(
                                        height: Dimensions.height40,
                                        width: Dimensions.screenWidth,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(Dimensions.radius30),
                                          color:Color(0xFF021B52)
                                        ),
                                        child: TextButton(
                                          onPressed: () {},
                                          child: Text("Book now",style: TextStyle(color: Colors.white,fontSize: Dimensions.font20),),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    );
                  },
                  child: Text(
                    "View",
                    style: TextStyle(
                        fontSize: Dimensions.font20, color: Colors.white,),
                  )),
            ),
          ),
        ],
      ),
    );
  }
}
